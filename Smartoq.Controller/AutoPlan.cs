using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using System.Threading;
using Microsoft.SPOT.Hardware;

namespace Smartoq.Controller
{
    public class AutoPlan
    {
        bool isZone1 = true;

        Device device;

        DeviceOutputPort[] zones; 
        
        DeviceOutputPort pump;

        int currentZone;

        public AutoPlan(Device device, int zoneCount, bool netduino)
        {
            this.device = device;
            Cpu.Pin[] pins;
            Cpu.Pin GPIO_Pin16 = (Cpu.Pin) 16;
           

            if (netduino)
            {
                pins = new Cpu.Pin[] { Pins.GPIO_PIN_D0, Pins.GPIO_PIN_D1, Pins.GPIO_PIN_D2, Pins.GPIO_PIN_D3, Pins.GPIO_PIN_D4, Pins.GPIO_PIN_D5, Pins.GPIO_PIN_D6,
                         Pins.GPIO_PIN_D7};


                pump = new DeviceOutputPort(device, Pins.GPIO_PIN_A4, false);
            }
            else
            {
                //0 = rx, 1 =tx, so ingore it
                pins = new Cpu.Pin[] { Cpu.Pin.GPIO_Pin14, Cpu.Pin.GPIO_Pin15, Cpu.Pin.GPIO_Pin2, Cpu.Pin.GPIO_Pin3, Cpu.Pin.GPIO_Pin4, Cpu.Pin.GPIO_Pin5, Cpu.Pin.GPIO_Pin6, Cpu.Pin.GPIO_Pin7 };


                pump = new DeviceOutputPort(device, GPIO_Pin16, false);
            }

            zones = new DeviceOutputPort[zoneCount];

            for (int i = 0; i < zoneCount; i++)
            {
                zones[i] = new DeviceOutputPort(device, pins[i], true);
            }
        }

        public void Execute(Scheduler scheduler = null )
        {
            if (scheduler == null)
                scheduler = new Scheduler(DateTime.UtcNow, new TimeSpan(0, 0, 10), new TimeSpan(0, 0, 15), 0);

            scheduler.JobStarted += scheduler_JobStarted;
            scheduler.DurationReached += scheduler_DurationReached;
            scheduler.Start();

        }

        //relay needs to set high to close
        void scheduler_DurationReached(DateTime datetime, int count)
        {
            

            Debug.Print("Pump off");
            pump.Write(false);
            Thread.Sleep(1000);

            Debug.Print("Zone " + currentZone + " off");
            zones[currentZone].Write(true);
            Debug.Print("Zone " + currentZone + " is off");
            RotateZone();
        }


        void scheduler_JobStarted(DateTime datetime, int count)
        {
            Debug.Print("Zone " + currentZone + " is on");
            zones[currentZone].Write(false);
            Thread.Sleep(1000); // delay for pump to start
            Debug.Print("Pump On");
            pump.Write(true);
        }

        private void RotateZone()
        {
            currentZone++;
            if (currentZone >= zones.Length)
            {
                currentZone = 0;
            }
        }


    }
}
