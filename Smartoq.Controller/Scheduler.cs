using System;
using Microsoft.SPOT;
using System.Threading;

namespace Smartoq.Controller
{

    public delegate void JobStartedHandler( DateTime datetime, int count);
    public delegate void JobDurationReachedHandler(DateTime datetime, int count);
    public class Scheduler
    {
        public int Starting { get; set; }
        public int MaxDuration { get; set; }
        public int Interval { get; set; }
        public int MaxCount { get; set; }
        public int Count { get; set; }

        public event JobStartedHandler JobStarted;
        public event JobDurationReachedHandler DurationReached;


        Thread thread;

        public Scheduler( SchedulerInfo schedulerInfo)
        {
            Starting = System.Math.Max((schedulerInfo.Starting - DateTime.UtcNow).TotalSeconds(), 0) * 1000;
            MaxDuration = schedulerInfo.MaxDuration.TotalSeconds() * 1000;
            Interval = schedulerInfo.Interval.TotalSeconds() * 1000;
            MaxCount = schedulerInfo.MaxCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="startingTime"></param>
        /// <param name="maxDuration">Duration of each zone</param>
        /// <param name="interval">Interval between jobs. </param>
        /// <param name="maxCount">0 - infinity</param>
        public Scheduler(DateTime startingTime, TimeSpan maxDuration, TimeSpan interval, int maxCount)
        {
            if (maxDuration >= interval || maxCount < 0)
                throw (new Exception("Invalid parameters."));
            //make sure the number is not negative
            Starting = System.Math.Max( (startingTime - DateTime.UtcNow).TotalSeconds(), 0) * 1000;
            MaxDuration = maxDuration.TotalSeconds() * 1000;
            Interval = interval.TotalSeconds() * 1000;
            MaxCount = maxCount;
        }

        public void Start()
        {
            thread = new Thread(new ThreadStart(Execute));
            thread.Start();
        }

        protected void Execute()
        {

            Thread.Sleep(Starting);
            while (MaxCount == 0 || Count < MaxCount)
            {
                Count++;

                DateTime startTime = DateTime.UtcNow;
                if (JobStarted != null)
                    JobStarted(startTime, this.Count);

                DateTime endTime = DateTime.UtcNow;
                int duration = (endTime - startTime).TotalSeconds() * 1000;

                Thread.Sleep( System.Math.Max( MaxDuration - duration, 0));

                if (DurationReached != null)
                    DurationReached(DateTime.UtcNow, this.Count);

                DateTime finishedTime = DateTime.UtcNow;
                
                Thread.Sleep( System.Math.Max(  Interval - (finishedTime - startTime).TotalSeconds() * 1000, 0)   );
            }
        }

    }
}
