using System;

using Microsoft.SPOT;
using Smartoq.Controller.NRF24L01Plus;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using System.Threading;

namespace Smartoq.Controller
{
    /// <summary>
    /// 
    /// 
    /// </summary>
    public class Gateway
    {
        const int timeout = 2000;
        const byte SystemCommand = 0;
        const byte PinCommand = 1;
        const byte StringCommand = 2;
        const byte BinaryCommand = 3;
        const byte CallCommand = 4;
        const byte CallCommandResponse = 5;
        
        byte[] localAddressBase;

        Device[] devices = new Device[6];

        Smartoq.Controller.NRF24L01Plus.NRF24L01Plus radio;
        byte[] address;
        byte channel;


        public Gateway( byte channel)
            : this("gtway", channel)
        {

        }

        public Gateway(): this("gtway", 76)
        {
           
        }

        public Gateway(byte[] address, byte channel)
        {
            this.address = address;
            this.channel = channel;
            Init();
        }

        public Gateway(string address, byte channel)
        {
            this.address = address.AsBytes();
            this.channel = channel;
            Init();
        }


        private void Init()
        {
            localAddressBase = "slot0".AsBytes();
            radio = new Smartoq.Controller.NRF24L01Plus.NRF24L01Plus();
            radio.Initialize(Microsoft.SPOT.Hardware.SPI.SPI_module.SPI1, Pins.GPIO_PIN_D10, Pins.GPIO_PIN_D9, Pins.GPIO_PIN_D8);
            radio.Configure( address, this.channel);
            //radio.SetAddress(AddressSlot.Zero, address);
            radio.OnDataReceived += Radio_OnDataReceived;
            radio.Execute(Commands.W_REGISTER, Registers.RF_SETUP, new byte[] { (byte)(1 << Bits.RF_DR_LOW | 3 << Bits.RF_PWR) });
            radio.Enable();
        }

        public Device RegisterDevice(AddressSlot slot, byte[] address, string name)
        {
            if (slot != AddressSlot.Zero)
            {
                byte[] newLocalAddress = new byte[5];
                localAddressBase.CopyTo(newLocalAddress, 0);
                newLocalAddress[4] = (byte) (48 + slot - 10 ); // character 0 = ascII 48

                Device device = new Device()
                {
                    AddressSlot = slot,
                    Address = address,
                    Name = name,
                    Gateway = this,
                    LocalPipeAddress = newLocalAddress
                };

                devices[(int)slot - 10] = device;

                radio.SetAddress(slot, newLocalAddress);

                return device;
            }
            else
            {
                throw (new Exception("Address slot address is reserved for ACK channel."));
            }
        }


        public Device RegisterDevice(AddressSlot slot, string address, string name)
        {
            return RegisterDevice(slot, address.AsBytes(), name);
        }

        public bool WriteText( byte[] deviceAddress, string text)
        {
            byte[] textBytes = text.AsBytes();
            byte[] bytes = new byte[ textBytes.Length + 1];
            bytes[0] = StringCommand ;
            Array.Copy(textBytes, 0, bytes, 1, textBytes.Length);
            return radio.SendTo(deviceAddress, bytes, timeout);
        }

        public bool WriteToOutput(byte[] deviceAddress , Microsoft.SPOT.Hardware.Cpu.Pin pin, bool state)
        {

            return radio.SendTo(deviceAddress, new byte[] { PinCommand, (byte)pin, (byte)(state ? 1 : 0) }, timeout);
        }

        public bool WriteBinaryCommand(byte[] deviceAddress, byte[] commandBytes)
        {
            byte[] bytes = new byte[commandBytes.Length + 1];
            bytes[0] = BinaryCommand;
            Array.Copy(commandBytes, 0, bytes, 1, commandBytes.Length);
            return radio.SendTo(deviceAddress, bytes, timeout);
        }

        public bool WriteCallCommandResponse(byte[] deviceAddress, byte[] commandBytes)
        {
            byte[] bytes = new byte[commandBytes.Length + 1];
            bytes[0] = CallCommandResponse;
            Array.Copy(commandBytes, 0, bytes, 1, commandBytes.Length);
            return radio.SendTo(deviceAddress, bytes, timeout);
        }

        public bool WriteSystemCommand(byte[] deviceAddress, byte[] commandBytes)
        {
            byte[] bytes = new byte[commandBytes.Length + 1];
            bytes[0] = SystemCommand;
            Array.Copy(commandBytes, 0, bytes, 1, commandBytes.Length);
            return radio.SendTo(deviceAddress, bytes, timeout);
        }



        byte[] callResult;

        public byte[] Call( byte[] deviceAddress, byte[] commandBytes)
        {

            byte[] bytes = new byte[commandBytes.Length + 1];
            bytes[0] = CallCommand;
            Array.Copy(commandBytes, 0, bytes, 1, commandBytes.Length);
            if (radio.SendTo(deviceAddress, bytes, timeout))
            {
                DateTime expires = DateTime.UtcNow.AddMilliseconds(timeout);
                while(DateTime.UtcNow < expires)
                {
                    if (callResult != null)
                        return callResult;
                    else
                        Thread.Sleep(100);
                }
                //throw (new Exception("Call timeout."));
                Debug.Print("Call timeout");
                return null;
            }
            else
            {
                //throw(new Exception("Call failed."));
                Debug.Print("Call failed.");
                return null;
            }
        }

        void Radio_OnDataReceived(byte[] data, byte dataPipe)
        {
            var device = devices[(int)dataPipe];

            //foreach( byte b in data)
            //{
            //    Debug.Print(b.ToString());
            //}


            if (device == null)
            {
                Debug.Print("no device found for pipe " +  dataPipe);
            }
            else
            {
                switch (data[0])
                {
                    case SystemCommand:
                        if (data[1] == 1)
                            SyncTime(devices[(int)dataPipe].Address);
                        break;
                    case StringCommand:
                            device.OnStringCommandReceived(data.AsString( 1 ));
                        break;

                    case BinaryCommand:
                        byte[] bytes = new byte[ data.Length - 1 ];
                        Array.Copy( data, 1, bytes, 0, data.Length -1);
                        device.OnBinaryCommandReceived(bytes);
                        break;
                    case PinCommand:
                        device.OnPinCommandReceived(data[1], data[2] != 0);
                        break;
                    case CallCommand:
                        byte[] callData = new byte[ data.Length - 1 ];
                        Array.Copy(data, 1, callData, 0, data.Length - 1);
                        device.OnCallCommandReceived(callData);
                        break;
                    case CallCommandResponse:
                        byte[] response = new byte[ data.Length - 1 ];
                        Array.Copy(data, 1, response, 0, data.Length - 1);
                        this.callResult = response;
                        break;
                }
            }

        }

        internal void SyncTime(byte[] address)
        {
            DateTime now = DateTime.Now;

            byte[] array = { 0, (byte)now.Hour, (byte)now.Minute, (byte)now.Second, (byte)now.Day, (byte)now.Month, (byte)(now.Year - 2010) };

            WriteSystemCommand(address, array);
        }
    }
}