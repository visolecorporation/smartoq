using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace Smartoq.Controller
{
    public class DeviceOutputPort 
    {
        Device device;
        Microsoft.SPOT.Hardware.Cpu.Pin pin;
        OutputPort localPort;

        public DeviceOutputPort(Device device, Cpu.Pin pin, bool initialState) 
        {
            if (device == null)
            {

                Debug.Print("Local port with pin:" + pin);
                localPort = new OutputPort(pin, initialState);
                Debug.Print("Local port initialized.");
                
            }
            else
            {

                Debug.Print("Remote port with pin:" + pin);
                if (device.WriteToOutput(pin, initialState))
                {
                    Debug.Print("Remote port initialized.");
                }
                else
                {
                    string s = "Remote port failed to initialize.";
                    Debug.Print(s);
                    throw (new Exception(s));
                }
            }

            this.device = device;
            this.pin = pin;
        }

        public bool Write(bool state)
        {
            if (device == null)
            {
                localPort.Write(state);
                return true;
            }
            else
                return device.WriteToOutput(pin, state);
        }

    }
}
