using System;
using Microsoft.SPOT;

namespace Smartoq.Controller
{
    public class SchedulerInfo
    {
        public DateTime Starting { get; set; }
        public TimeSpan MaxDuration { get; set; }
        public TimeSpan Interval { get; set; }
        public int MaxCount { get; set; }
    }
}
