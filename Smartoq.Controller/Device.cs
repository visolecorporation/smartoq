using System;
using Microsoft.SPOT;
using Smartoq.Controller.NRF24L01Plus;
using System.Threading;

namespace Smartoq.Controller
{
    public delegate void BinaryCommandReceiveHandler( byte[] bytes);
    public delegate void StringCommandReceiveHandler(string text);
    public delegate void PinCommandReceiveHandler(byte outputpin, bool state);
    public delegate byte[] CallCommand(byte[] bytes);


    public class Device
    {
        public string Name { get; set; }
        public event BinaryCommandReceiveHandler BinaryCommandReceived;
        public event StringCommandReceiveHandler StringCommandReceived;
        public event PinCommandReceiveHandler PinCommandReceived;
        public event CallCommand CallCommandReceived;

        public byte[] Address { get; set; }
        public byte[] LocalPipeAddress { get; set; }
        public AddressSlot AddressSlot { get; set; }

        public Gateway Gateway { get; set; }
        private bool ready;


        public void ensureChannelReady()
        {
            if (ready)
                return;
            else
            {
                while (!ready)
                {
                    Thread.Sleep(100);
                }
            }
        }

        internal void OnBinaryCommandReceived(byte[] data)
        {
            if (BinaryCommandReceived != null)
                BinaryCommandReceived(data);
        }
        internal void OnCallCommandReceived(byte[] data)
        {
            if (CallCommandReceived != null)
            {
                byte[] result = CallCommandReceived(data);
                Gateway.WriteCallCommandResponse(this.Address, result);
            }
        }

        internal void OnStringCommandReceived(string text)
        {
            if (StringCommandReceived != null)
                StringCommandReceived(text);
        }
        internal void OnPinCommandReceived(byte pin, bool state)
        {
            if (PinCommandReceived != null)
                PinCommandReceived(pin, state);
        }


        public bool WriteToOutput(Microsoft.SPOT.Hardware.Cpu.Pin pin, bool state)
        {
            //ensureChannelReady();
            ready = Gateway.WriteToOutput(this.Address, pin, state);
            return ready;
        }

        public bool WriteBinaryCommand(byte[] command)
        {
            //ensureChannelReady();
            ready = Gateway.WriteBinaryCommand(this.Address, command);
            return ready;
        }

        public bool WriteText(string text)
        {
            //ensureChannelReady();
            ready = Gateway.WriteText( this.Address, text);
            return ready;
        }

        public byte[] Call(byte[] callData)
        {
            return Gateway.Call(this.Address, callData);
        }

        internal void SyncTime()
        {
            Gateway.SyncTime(this.Address);

        }
    }
}
