using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using System.Threading;

namespace Smartoq.Controller
{   

    public class RemotePlan
    {

        const byte ZoneBinaryCommand = 0;
        const byte StatusRequestBinaryCommand = 1;
        const byte StatusResponseBinaryCommand = 2;
        const byte ZoneResetCommand = 3;

        Device device;

        int currentZone = 1;
        int zoneCount;
        TimeSpan zoneDuration;
        Scheduler scheduler;

        public RemotePlan(Device device, int zoneCount, TimeSpan zoneDuration)
        {
            this.device = device;
            this.zoneCount = zoneCount;
            this.zoneDuration = zoneDuration;
        }

        public void Execute(Scheduler scheduler = null )
        {
            if (scheduler == null)
                scheduler = new Scheduler(DateTime.UtcNow, new TimeSpan(0, 0, 10), new TimeSpan(0, 0, 15), 0);

            this.scheduler = scheduler;

            byte[] command = new byte[1];
            command[0] = ZoneResetCommand;
            device.WriteBinaryCommand(command);

            scheduler.JobStarted += scheduler_JobStarted;
            scheduler.DurationReached += scheduler_DurationReached;
            scheduler.Start();

        }

        //relay needs to set high to close
        void scheduler_DurationReached(DateTime datetime, int count)
        {   
            byte[] command = new byte[1];
            command[0] = ZoneResetCommand;
            device.WriteBinaryCommand(command);
        }

        void CheckStatus()
        {
            byte[] callData = new byte[] { StatusRequestBinaryCommand };

            var result = device.Call(callData);

            if (result != null)
                Debug.Print("Status: last zone -" + result[0] + " status -" + result[1]);
        }


        void scheduler_JobStarted(DateTime datetime, int count)
        {

            byte[] command = new byte[3];

            for( int i = 0; i < zoneCount; i++)
            {
                command[0] = ZoneBinaryCommand;
                command[1] = (byte)this.currentZone;
                command[2] = 1;
                device.WriteBinaryCommand(command);

                Thread.Sleep(zoneDuration.TotalSeconds() * 1000);

                command[0] = ZoneBinaryCommand;
                command[1] = (byte)this.currentZone;
                command[2] = 0;
                device.WriteBinaryCommand(command);
                RotateZone();

            }
            
        }

        private void RotateZone()
        {
            currentZone++;
            if (currentZone > zoneCount)
            {
                currentZone = 1;
            }
        }


    }
}
