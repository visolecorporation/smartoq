﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using System.Text;
using Smartoq.Controller.NRF24L01Plus;

namespace Smartoq.Controller
{
    public class Program
    {


        static InputPort button = new InputPort(Pins.ONBOARD_SW1, false, Port.ResistorMode.Disabled);

        static bool planStarted = false;

        private static void ExecuteLocalPlan()
        {
            AutoPlan plan = new AutoPlan(null, 8, true);
            plan.Execute( new Scheduler( DateTime.Now, new TimeSpan(0, 20, 0), new TimeSpan( 48, 0, 0), 0 )  );
        }




        public static void Main()
        {

            //TestRadio();

            //ExecuteLocalPlan();

            ///F0F0F0F0FF
           //TestGateway();


            TestRemotePlan();

            Thread.Sleep(Timeout.Infinite);
            

        }

        private static void TestRemotePlan()
        {
            Gateway gateway = new Gateway(76);

            Device device = gateway.RegisterDevice(AddressSlot.One, "sprkl", "Sprinkler");
            device.BinaryCommandReceived +=device_BinaryCommandReceived;

            while (true)
            {
                if (button.Read() == true && planStarted == false)
                {
                    planStarted = true;

                    try
                    {
                        //device.SyncTime();
                        Debug.Print("Current time is " + DateTime.Now.ToString());

                        var plan = new RemotePlan(device, 7, new TimeSpan(0,0,10));

                        Scheduler schedule = new Scheduler(DateTime.UtcNow.AddMinutes(1), new TimeSpan(0, 2, 0), new TimeSpan(0, 5, 0), 0);
                        plan.Execute(schedule);
                    }
                    catch
                    {

                    }

                    Thread.Sleep(100);
                }
                else
                {
                    Thread.Sleep(1000);
                }

            }
        }

        static void device_BinaryCommandReceived(byte[] bytes)
        {

            foreach (byte b in bytes)
            {
                Debug.Print(b.ToString());
            }
        }

        private static void TestGateway()
        {
            Gateway gateway = new Gateway(76);

            Device device = gateway.RegisterDevice(AddressSlot.One, "sprkl", "Sprinkler");
            //gateway.RegisterDevice(AddressSlot.Zero, "sprkl", "Sprinkler");

            device.StringCommandReceived += device_StringCommandReceived;
            //int i = 0;
            //while (true)
            //{
            //    if (device.WriteText("hello" + i))
            //    {
            //        Debug.Print("cool");
            //    }
            //    else
            //        Debug.Print("not cool");

            //    i++;

            //    Thread.Sleep(1000);
            //}
        }

        private static void TestRadio()
        {
            Smartoq.Controller.NRF24L01Plus.NRF24L01Plus radio = new Smartoq.Controller.NRF24L01Plus.NRF24L01Plus();
            
            radio.Initialize(Microsoft.SPOT.Hardware.SPI.SPI_module.SPI1, Pins.GPIO_PIN_D10, Pins.GPIO_PIN_D9, Pins.GPIO_PIN_D8);
            //radio.Configure(new byte[] { 255, 240, 240, 240, 240}, 100);
            radio.Configure("gtway".AsBytes(), 76);
            radio.OnDataReceived += Radio_OnDataReceived;
            radio.Execute(Commands.W_REGISTER, Registers.RF_SETUP, new byte[] { (byte)(1 << Bits.RF_DR_LOW | 3 << Bits.RF_PWR) });
            radio.Enable();

            //while (true)
            //{
            //    if (radio.SendTo("sprkl".AsBytes(), "hello".AsBytes(), 10))
            //        Thread.Sleep(1000);
            //    else
            //        Debug.Print("no receipt");
            //}

        }

        private static void Radio_OnDataReceived(byte[] data, byte dataPipe)
        {
            Debug.Print("dataPipe" + dataPipe);

            foreach (byte b in data)
            {
                Debug.Print(b.ToString());
            }

        }

        static void device_StringCommandReceived(string text)
        {
            Debug.Print(text);
        }


    }
}
