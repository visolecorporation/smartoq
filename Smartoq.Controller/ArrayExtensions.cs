using System;
using Microsoft.SPOT;

namespace Smartoq.Controller
{
    public static class ArrayExtensions
    {
        public static byte[] Reverse( this byte[] array)
        {
            byte[] buffer = new byte[array.Length];
            for (int i = 0; i < array.Length; i++)
                buffer[i] = array[array.Length - (i + 1)];
            return buffer;
        }

    }
}
