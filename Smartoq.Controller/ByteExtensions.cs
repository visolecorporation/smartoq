using System;
using Microsoft.SPOT;

namespace Smartoq.Controller
{
    public static class ByteExtensions
    {
        public static string AsString(this byte[] data)
        {
            return new string(System.Text.UTF8Encoding.UTF8.GetChars(data, 0, data.Length));
        }

        public static string AsString(this byte[] data, int offSet)
        {
            return new string(System.Text.UTF8Encoding.UTF8.GetChars(data, offSet, data.Length - offSet));
        }

        public static byte[] AsBytes(this string text)
        {
            return System.Text.UTF8Encoding.UTF8.GetBytes(text);
        }

        public static string ToHexBitString(this byte[] bytes)
        {
            char[] c = new char[bytes.Length * 2];
            int b;
            for (int i = 0; i < bytes.Length; i++)
            {
                b = bytes[i] >> 4;
                c[i * 2] = (char)(55 + b + (((b - 10) >> 31) & -7));
                b = bytes[i] & 0xF;
                c[i * 2 + 1] = (char)(55 + b + (((b - 10) >> 31) & -7));
            }
            return new string(c);
        }
    }
}
