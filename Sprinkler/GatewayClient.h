// GatewayClient.h

#include "RF24.h"
#include "SPI.h"
#include "RF24_config.h"
#include "nRF24L01.h"

#include "TimeAlarms.h"
#include "Time.h"

#ifndef _GATEWAYCLIENT_h
#define _GATEWAYCLIENT_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif


typedef bool(*OutputCommandCallback) (uint8_t pin, bool state);

typedef bool(*TextCallback) (char* text);

typedef bool(*BinaryCommandCallback) (byte bytes[]);

typedef byte* (*CallCommandCallback) (byte bytes[]);

class GatewayClient
{
 protected:

	 void reverseBytes(unsigned char *start, int size);

 public:
	void init(byte gatewayAddress[], byte clientAddress[], byte channel);
	void loop();
	void writeBinaryCommand(byte data[]);
	void writeCallCommandResponse(byte data[]);
	void writeText(char* text);
	void writeRawBytes(byte data[], int lenth);
	OutputCommandCallback handleOutput;
	TextCallback handleText;
	BinaryCommandCallback handleBinaryCommand;
	CallCommandCallback handleCallCommand;
};


#endif

