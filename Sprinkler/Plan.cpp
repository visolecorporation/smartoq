#include "TimeAlarms.h"
#include "Time.h"
#include "SprinklerManager.h"
#include "Plan.h"


void Plan::setManager(SprinklerManager* sprinklerManager)
{
	this->manager = sprinklerManager;
}

void Plan::init(uint8_t zoneCount)
{
	totalZoneCount = zoneCount;

	if (manager->isZoneOn())
		manager->executeZone(manager->getCurrentZone(), false);

	currentZone = 0;
}

void Plan::disable()
{
	for (int i = 0; i < totalZoneCount * 2; i++)
	{
		Alarm.disable(this->zoneTimers[i]);
	}
}

void Plan::enable()
{
	for (int i = 0; i < totalZoneCount * 2; i++)
	{
		Alarm.enable(this->zoneTimers[i]);
	}
}

void Plan::executeZone()
{
	
	if (manager->isZoneOn())
	{
		manager->executeZone(currentZone, false);
	}
	else
	{
		currentZone++;
		if (currentZone > totalZoneCount)
			currentZone = 1;
		manager->executeZone(currentZone, true);
	}

	Serial.println(manager->getCurrentZone());
	Serial.println(manager->isZoneOn());

	lastFinishTime = now();
}

