// SprinklerManager.h

#ifndef _SPRINKLERMANAGER_h
#define _SPRINKLERMANAGER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

class SprinklerManager
{
 protected:


 public:
	void init();
	bool executeZone(uint8_t zone, bool isOn);
	bool commandZoneByPin(uint8_t zonePin, bool isOn);
	bool closeAll();
	void reset();
	uint8_t NONE_ZONE;
	uint8_t getCurrentZone();
	bool isZoneOn();
private:
 	uint8_t PUMP;
	bool isOn;
	uint8_t currentZone;
};
#endif

