
#include "SprinklerManager.h"
#include "TimeAlarms.h"
#include "Time.h"


void SprinklerManager::init()
{
	PUMP = 13;
	NONE_ZONE = 0;

	pinMode(11, OUTPUT);
	digitalWrite(11, HIGH);

	pinMode(12, OUTPUT);
	digitalWrite(12, HIGH);
		
	pinMode(2, OUTPUT);
	digitalWrite(2, HIGH);

	pinMode(3, OUTPUT);
	digitalWrite(3, HIGH);

	pinMode(4, OUTPUT);
	digitalWrite(4, HIGH);

	pinMode(5, OUTPUT);
	digitalWrite(5, HIGH);

	pinMode(6, OUTPUT);
	digitalWrite(6, HIGH);

	//pinMode(7, OUTPUT);
	//digitalWrite(7, HIGH);
}


void SprinklerManager::reset()
{
	if (currentZone != NONE_ZONE)
		executeZone(currentZone, false);

}


bool SprinklerManager::closeAll()
{
	
	digitalWrite(PUMP, LOW);
	digitalWrite(11, HIGH);
	digitalWrite(12, HIGH);
	digitalWrite(2, HIGH);
	digitalWrite(3, HIGH);
	digitalWrite(4, HIGH);
	digitalWrite(5, HIGH);
	digitalWrite(6, HIGH);
	//digitalWrite(7, HIGH);
	return true;
}

bool SprinklerManager::executeZone(uint8_t zone, bool status)
{
	printf("zone %u, %i \n", zone, status);

	switch (zone)
	{
	case 1:
		if (commandZoneByPin(11, status))
			currentZone = 1;
		else
			currentZone = NONE_ZONE;
		break;
	case 2:
		if (commandZoneByPin(12, status))
			currentZone = 2;
		else
			currentZone = NONE_ZONE;
		break;
	default:
		if (commandZoneByPin(zone - 1, status))
			currentZone = zone;
		else
			currentZone = NONE_ZONE;
		break;
	}

	this->isOn = status;
	currentZone = zone;

}

bool SprinklerManager::commandZoneByPin(uint8_t zonePin, bool status)
{
	if (status)
	{
		digitalWrite(zonePin, LOW);
		Alarm.delay(1000);
		digitalWrite(PUMP, HIGH);
	}
	else
	{

		digitalWrite(PUMP, LOW);
		Alarm.delay(1000);
		digitalWrite(zonePin, HIGH);
	}

	return status;
}

uint8_t SprinklerManager::getCurrentZone()
{
	return currentZone;
}

bool SprinklerManager::isZoneOn()
{
	return isOn;
}