// Plan.h
#include "SprinklerManager.h"

#ifndef _PLAN_h
#define _PLAN_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

class Plan
{
 protected:


 public:
	AlarmID_t zoneTimers[16];
	void init(uint8_t zoneCount);
	void disable();
	void enable();
	void setManager( SprinklerManager* sprinklerManager );
	void executeZone();
private:
	SprinklerManager* manager;
	time_t lastFinishTime;
	uint8_t currentZone;
	uint8_t totalZoneCount;
};

#endif

