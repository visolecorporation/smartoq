
#include <SoftSPI.h>
#include <RF24_config.h>
#include <printf.h>
#include <PinIO.h>
#include <nRF24L01.h>
#include <I2cConstants.h>
#include <DigitalIO.h>
#include <SoftI2cMaster.h>
#include <DigitalPin.h>
#include <TimeAlarms.h>
#include <Time.h>
#include "Plan.h"
#include "SprinklerManager.h"
#include "GatewayClient.h"
#include <SPI.h>







GatewayClient* gatewayClient;
byte addresses[][6] = { "slot1", "sprkl" };
SprinklerManager* manager;
Plan* plan;

const byte ZoneBinaryCommand = 0;

const byte StatusRequestBinaryCommand = 1;
const byte StatusResponseBinaryCommand = 2;
const byte ZoneResetCommand = 3;

int defaultScheduler;

void setup() {
		
	setTime(0, 0, 0, 6, 11, 14);
	Serial.begin(9600);
	printf_begin();
	displayCurrentTime();


	//setupDefaultPlan();

	manager = new SprinklerManager();
	manager->init();

	gatewayClient = new GatewayClient();
	gatewayClient->init(addresses[0], addresses[1], 76);
	gatewayClient->handleOutput = handleOutputCommand;
	gatewayClient->handleText = handleText;
	gatewayClient->handleBinaryCommand = handleBinaryCommand;
	gatewayClient->handleCallCommand = handleCallCommand;

}

//
void loop(){

	gatewayClient->loop();

/*
	byte b[2];
	b[0] = 1;
	b[1] = 2;
	gatewayClient->writeBinaryCommand(b);*/

	Alarm.delay(500);
}


bool handleOutputCommand(uint8_t pin, bool state)
{

	printf("pin: %i %d", pin, state);

	return true;
}

bool handleText(char* text)
{
	printf(text);

	return true;
}


byte* handleCallCommand(byte* commandBytes)
{
	switch (commandBytes[0])
	{
	case ZoneResetCommand:
		manager->closeAll();
		break;
	case StatusRequestBinaryCommand:
		printf("handle status request");

		byte* data;
		data = new byte[2];
		data[0] = manager->getCurrentZone();
		data[1] = manager->isZoneOn();
		return data;
		break;
	default:
		printf("no matching functional call");
		break;
	}

	return NULL;
}

bool handleBinaryCommand(byte* commandBytes)
{
	switch (commandBytes[0])
	{
		case ZoneBinaryCommand:
			// zone on, off;
			uint8_t zone;
			zone = commandBytes[1];
			bool isOn;
			isOn = (commandBytes[2] != 0);
			manager->closeAll();
			manager->executeZone(zone, isOn);
			break;
		default:
			break;
	} 
	
	return true;
}

void setupDefaultPlan()
{
	printf("setup plan\n");
	uint8_t zoneCount = 8;

	plan = new Plan();
	plan->init(zoneCount);
	int startingHour = 0;
	int durationInMinutes = 20;
	
	for (int i = 0; i < zoneCount; i++)
	{	
		int offset = durationInMinutes * i;
		plan->zoneTimers[i * 2] = Alarm.alarmRepeat(startingHour + offset / 60, offset % 60, 5, executePlanZone);
		printf("setup schedule for zone %d -- %d:%d:%d\n",i, startingHour + offset / 60, offset % 60, 5);
		plan->zoneTimers[i * 2 + 1] = Alarm.alarmRepeat(startingHour + (offset + durationInMinutes) / 60, (offset + durationInMinutes) % 60, 0, executePlanZone);
		
	}
}

void executePlanZone()
{
	plan->executeZone();
}

void displayCurrentTime()
{
	time_t t = now();
	printf("Current time is %d %d-%d-%d %d:%d:%d\n", weekday(t), year(t), month(t), day(t), hour(t), minute(t), second(t));
}