
#include "GatewayClient.h"

const byte PIN_COMMAND = 1;
const byte TEXT_COMMAND = 2;
const byte BINARY_COMMAND = 3;
const byte CALL_COMMAND = 4;
const byte CALL_COMMAND_RESPONSE = 5;
const byte SYSTEM_COMMAND = 0;


RF24* rf24 = new RF24(9, 10);

void GatewayClient::init(byte gatewayAddress[], byte clientAddress[], byte channel)
{

	rf24->begin();                           // Setup and configure rf rf24
	rf24->setChannel(channel);
	rf24->setPALevel(RF24_PA_MAX);
	rf24->setDataRate(RF24_250KBPS);
	rf24->enableDynamicPayloads();
	
	rf24->setRetries(15, 15);                   // Optionally, increase the delay between retries & # of retries

	//this->reverseBytes(gatewayAddress, 5);
	//this->reverseBytes(clientAddress, 5);





	uint64_t gnumber = *((uint64_t*)gatewayAddress);
	uint64_t cnumber = *((uint64_t*)clientAddress);


	rf24->openWritingPipe(gnumber);
	rf24->openReadingPipe(1, cnumber);


	rf24->startListening();                 // Start listening
	rf24->printDetails();                   // Dump the configuration of the rf unit for debugging

}

void GatewayClient::reverseBytes(unsigned char *start, int size) {
	unsigned char *lo = start;
	unsigned char *hi = start + size - 1;
	unsigned char swap;
	while (lo < hi) {
		swap = *lo;
		*lo++ = *hi;
		*hi-- = swap;
	}
}

int bytes = 0;

void GatewayClient::loop()
{
	byte rxBuffer[32];
	String* text;

	bool hasData = false;

	while (rf24->available())
	{
		hasData = true;
		// read from rf24
		int len = rf24->getDynamicPayloadSize();

		if (len != 0)
		{

			bytes += len;

			rf24->read(&rxBuffer, len);

			

			switch (rxBuffer[0])
			{
			case PIN_COMMAND:
				handleOutput(rxBuffer[1], rxBuffer[2] != 0);
				break;
			case TEXT_COMMAND:

				rxBuffer[len] = '\0';

				// first byte of the buffer is command, but we also need an ending \0
				char* s;
				s = new char[len + 1];
				memcpy(s, rxBuffer + 1, len); // including the ending \0

				handleText(s);

				delete[] text;
				break;
			case BINARY_COMMAND:

				byte* commandBytes;
				commandBytes = new byte[len];
				memcpy(commandBytes, rxBuffer + 1, len - 1);
				this->handleBinaryCommand(commandBytes);
				delete[] commandBytes;
				break;
			case CALL_COMMAND:
				printf("Call command received.");
				byte* callBytes;
				callBytes = new byte[len];
				memcpy(callBytes, rxBuffer + 1, len - 1);


				for (int i = 0; i < len - 1; i++)
				{
					printf(" %#1x ", callBytes[i]);
				}
				printf("\n");

				byte* result;
				result = this->handleCallCommand(callBytes);

				printf("send call result.");
				for (int i = 0; i < 2; i++)
				{
					printf(" %#1x ", result[i]);
				}
				printf("\n");
				

				writeCallCommandResponse(result);
				delete[] callBytes;
				delete[] result;
				break;

			case SYSTEM_COMMAND:
				byte systemCommand;
				systemCommand = rxBuffer[1];

				if (systemCommand == 0) // time;
				{

					time_t t = now();
					printf("before time is %d %d-%d-%d %d:%d:%d\n", weekday(t), year(t), month(t), day(t), hour(t), minute(t), second(t));

					setTime(rxBuffer[2], rxBuffer[3], rxBuffer[4], rxBuffer[5], rxBuffer[6], rxBuffer[7] + 2010);

					t = now();
					printf("after time is %d %d-%d-%d %d:%d:%d\n", weekday(t), year(t), month(t), day(t), hour(t), minute(t), second(t));
				}

				break;
			default:
				printf("Unknown data.");
				break;
			}
		}
		else
		{
			printf("x");
		}

		if (!hasData)
		{
			printf(".");
		}
	}
}

void GatewayClient::writeBinaryCommand(byte data[])
{
	byte buf[32];
	buf[0] = BINARY_COMMAND;
	int len = sizeof(data);
	memcpy(buf + 1, data, len);
	this->writeRawBytes(buf, len + 1);
}
void GatewayClient::writeCallCommandResponse(byte data[])
{
	byte buf[32];
	buf[0] = CALL_COMMAND_RESPONSE;
	int len = sizeof(data);
	memcpy(buf + 1, data, len);
	this->writeRawBytes(buf, len + 1);
}

void GatewayClient::writeText(char* text)
{
	

	int len = strlen(text) + 1;


	byte newBytes[32];
	for (int i = 0; i < 32; i++)
	{
		newBytes[i] = 0;
	}

	newBytes[0] = TEXT_COMMAND;
	memcpy(newBytes + 1, text, len );

	this->writeRawBytes(newBytes, len + 1);
}

void GatewayClient::writeRawBytes(byte rawData[], int lenth)
{
	printf("writeRawBytes");
	rf24->stopListening();                                  // First, stop listening so we can talk.
	byte gotByte;
	unsigned long time = micros();                          // Take the time, and send it.  This will block until complete   
	//Called when STANDBY-I mode is engaged (User is finished sending)


	for (int i = 0; i < lenth; i++)
	{
		printf(" %#1x ", rawData[i]);
	}
	printf("\n");



	if (!rf24->write(rawData, lenth)){
	//if (!rf24->write(&text, 5)){
		printf("failed.\n\r");
	}
	else
	{

		if (!rf24->available())
		{
			printf("Blank Payload Received\n\r");
		}
		else
		{
			while (rf24->available())
			{
				unsigned long tim = micros();
				rf24->read(&gotByte, 1);
				printf("Got response %d, round-trip delay: %lu microseconds\n\r", gotByte, tim - time);
				break;
			}
		}
	}
	rf24->startListening();
}